from flask import Flask

UPLOAD_FOLDER = 'static/uploads/'

app = Flask(__name__) #Init Flask App
app.secret_key = "secret key"
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER #Local Server Directory
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024 * 100 # Max File size depending on Server Compute Power