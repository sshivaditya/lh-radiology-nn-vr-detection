# Kvasir VR Aplication

This application was created with WebXR support. The application loads the default video present in the public directory. For object detection I have used TF Object Detection API. And converted the model to TFJS format using the TFJS-Converter. The resulting model is stored in web_model folder

## Installation Instructions

* Clone the repository
* /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
* eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"
* brew install mkcert
* mkcert -install
* mkcert 0.0.0.0 localhost 127.0.0.1 ::1
* Rename 0.0.0.0+3-key.pem -> key.pem
* Rename 0.0.0.0+3.pem -> cert.pem
* http-server ./ -S -C cert.pem -o --cors
* run npm install
* run npm start
  
## VR Usage Instruction

* For Google Cardboard usage this requires Chromium 91 or greater
* The application is to be run on a local server which can forward the port to the local IP Address
* Memory Leak Issue is present

## Issues

* For frame rate greater than 40, GPU memory exceeds 2125 MB 
* Per Frame inference requires more than 20 MB
* TFJS-Core@3.7.0 has memory leak issues
* Best stable FPS value for android is 25 FPS consuming 1.7 GB beyond 25 WebGL Runs out of memory
  
## Platforms Tested

* Ubuntu 20.04 LTS Server
* Snapdragon 870 Mobile Phone
  

  