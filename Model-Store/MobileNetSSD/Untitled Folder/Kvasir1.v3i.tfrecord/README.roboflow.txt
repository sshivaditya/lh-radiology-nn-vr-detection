
Kvasir1 - v3 2021-07-04 6:46am
==============================

This dataset was exported via roboflow.ai on July 4, 2021 at 1:18 AM GMT

It includes 2400 images.
Polyps are annotated in Tensorflow TFRecord (raccoon) format.

The following pre-processing was applied to each image:
* Auto-orientation of pixel data (with EXIF-orientation stripping)
* Resize to 300x300 (Fit within)
* Auto-contrast via adaptive equalization

The following augmentation was applied to create 3 versions of each source image:
* Randomly crop between 0 and 20 percent of the image
* Random rotation of between -15 and +15 degrees
* Random Gaussian blur of between 0 and 3 pixels
* Salt and pepper noise was applied to 5 percent of pixels


