import tensorflowjs as tfjs
import tensorflow as tf
import numpy as np
import os
from utils import KvasirLabelImage
import model as md
import pretrained as pdm
import tensorflow_hub as hub
import argparse
import datetime


PATH = os.path.dirname(os.path.dirname(os.path.dirname(os.getcwd()))) + '/Input/' #PATH OF Dataset

data = KvasirLabelImage('/gsoc/Input/labeled-images/Images', 224, 224, 1, 0.2)


def pretrain():
    """ 
        For Use a Pre Trained Network and performing transfer learning
    """
    log_dirs = "logs/fit/" + 'PreTrain'
    tensorboard_callbacs = tf.keras.callbacks.TensorBoard(log_dir=log_dirs, histogram_freq=1)
    callback = tf.keras.callbacks.EarlyStopping(monitor='loss', patience=3)
    setpdm  = pdm.preTrained(len(data.train_ds.class_names))
    batch_stats_callback = pdm.CollectBatchStats()
    history = setpdm.model.fit(data.train_ds, epochs=1,
                        callbacks=[batch_stats_callback,callback,tensorboard_callbacs],validation_data=data.val_ds)
    tfjs.converters.save_keras_model(setpdm.model, 'tfjs_mods_pre/')


pretrain()